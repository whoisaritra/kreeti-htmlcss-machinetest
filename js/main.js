const sidebar = document.getElementById('sidebar');
const sidebarToogler = document.getElementById('sidebar-toogler');
sidebarToogler.addEventListener('click', function() {
    sidebar.classList.toggle('open');
})

const sidebarClose = document.getElementById('sidebar-close');
sidebarClose.addEventListener('click', function() {
    sidebar.classList.remove("open");
})
